set PROJECT_DIR=%~dp0

pushd "%PROJECT_DIR%lib\scripts"
grep --recursive --line-number --perl-regexp "(?<! = )EQUIPSLOTS\.BODY" > "%PROJECT_DIR%docs\grep-equipslots.body-1.txt"
grep --recursive --line-number --fixed-strings " = EQUIPSLOTS.BODY" > "%PROJECT_DIR%docs\grep-equipslots.body-2.txt"
popd

pause
