set PROJECT_DIR=%~dp0
set STEAM_LIB_DIR=%1
if "%STEAM_LIB_DIR%"=="" set STEAM_LIB_DIR=C:\Program Files (x86)\Steam
set DST_APP_DIR=%STEAM_LIB_DIR%\steamapps\common\Don't Starve Together

copy "%DST_APP_DIR%\data\databundles\scripts.zip" "%PROJECT_DIR%lib\scripts.zip" || goto :error
move "%PROJECT_DIR%lib\scripts" "%PROJECT_DIR%lib\scripts.bak" || goto :error
7z x "%PROJECT_DIR%lib\scripts.zip" -o"%PROJECT_DIR%lib" || goto :error
del "%PROJECT_DIR%lib\scripts.zip" || goto :error
rmdir /S /Q "%PROJECT_DIR%lib\scripts.bak" || goto :error

pushd "%PROJECT_DIR%lib\scripts"
grep --recursive --line-number --perl-regexp "(?<! = )EQUIPSLOTS\.BODY" > "%PROJECT_DIR%docs\grep-equipslots.body-1.txt"
grep --recursive --line-number --fixed-strings " = EQUIPSLOTS.BODY" > "%PROJECT_DIR%docs\grep-equipslots.body-2.txt"
popd

cat "%DST_APP_DIR%\version.txt"

:error
pause
