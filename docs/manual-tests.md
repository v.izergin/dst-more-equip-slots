## *Enlightened Crown*'s container

1. Get an *Enlightened Crown*

        c_give("alterguardianhat", 1)

2. Equip it.
3. Ensure that the container is positioned properly.


## *Turf-Raiser Helm*'s container

1. Get a *Turf-Raiser Helm*:

        c_give("antlionhat", 1)

2. Equip it.
3. Ensure that the container is positioned properly.


## *Bone Armor*'s fuel slot

1. Install and enable the mod [Item Auto-Fuel Slots](https://steamcommunity.com/sharedfiles/filedetails/?id=2736043172).
2. Run in the console:

        c_give("armorskeleton", 1)

3. Equip the *Bone Armor*.
4. Ensure that the fuel slot is positioned properly.


## Giving a coat to the *Crabby Hermit*

Giving a body insulation dress to the *Crabby Hermit* when snow is falling.

1.  Get two *Puffy Vest*s:

        c_give("trunkvest_winter", 2)

2.  Go to the *Crabby Hermit*:

        c_gonext("hermitcrab")

3.  Wait for the begining of a winter and a few days more (to get colder):

        TheWorld:PushEvent("ms_setseason", "winter")
        c_skip(2)

4.  Wait for a morning if needed:

        TheWorld:PushEvent("ms_nextcycle")

5.  Try to give her the coat. She should refuse with the two phrases:

    > You keep it, I'm not chilly.
    > I'd boil if I wore that in this heat!

6.  Start snowing:

        TheWorld:PushEvent("ms_forceprecipitation")

7.  Give her the coat. She should accept with the phrase:

    > Hmph. At least I won't freeze to death.

8.  Ensure that she has equipped the coat.
9.  Try to give her the second coat. She should refuse with the phrase:

    > I like the coat I have, thank you very much.

10. Stop snowing:

        TheWorld:PushEvent("ms_forceprecipitation", false)

11. Ensure that she has unequipped the coat.
12. Start snowing again:

        TheWorld:PushEvent("ms_forceprecipitation")

13. Ensure that she has equipped the coat again.
14. Optionally, go back to home in autumn:

        TheWorld:PushEvent("ms_forceprecipitation", false)
        TheWorld:PushEvent("ms_setseason", "autumn")
        c_gonext("multiplayer_portal")


## *Construction Amulet* in the crafting menu and recipe popup

1. Get a *Construction Amulet*:

        c_give("greenamulet", 1)

2. Open the crafting menu and select the receipt of *Log Suite*.
3. Equip the amulet.
4. Ensure that the icon of the amulet has appeared in the list of the ingredients.
5. Move the mouse over the *Rope* ingredient.
6. Ensure that there is the icon of the amulet in the list of the ingredients in the popup.


## *WhaRang Amulet*

1. Install end enable the mod [[DST] WhaRang](https://steamcommunity.com/sharedfiles/filedetails/?id=1108032281).
2. Disable the configuration option *Autodetect amulets*.
3. Get a WhaRang Amulet:

        c_give("wharang_amulet", 1)

4. Equip it.
5. Ensure that it has been unequipped and dropped.


## *Life Giving Amulet*

1. Get a *Life Giving Amulet*:

        c_give("amulet", 1)

2. Die:

        c_setheaalth(0)

3. Haunt the amulet.
4. Ensure that the amulet is displayed during the animation.
5. Ensure that the amulet is removed at the end.


## Costume on the stage

1. Set the configuration option *Costume* to *Extra body slot #1*.
2. Get a *Doll Costume* and a *Doll Mask*:

        c_give("costume_doll_body", 1)
        c_give("mask_dollhat", 1)

3. Go to the *Stage*:

        c_gonext("charlie_stage")

4. Equip the costume.
5. Try to perform on the stage. The birds should say:

   > Something's wrong!
   > Check your costumes!
   > We'll come back.
   > When you get it right!

6. Equip the mask.
7. Perform on the stage. The birds should say:

   > Act 1.
   > Yeah, scene 1!


## Pillows

1.  Set the *Forest* > *World Settings* > *Events* > *Year of the Bunnyman* to *Always*.

2.  Get the materials and build a *Bunnyman Shrine*:

        c_give("goldnugget", 4)
        c_give("board", 2)

3. Get a *Carrot* and give it to the shrine:

        c_give("carrot", 1)

4.  Get a *Pillow Fight Pit Kit* and plant it:

        c_give("yotr_fightring_kit", 1)

5.  Get a pillow and a pillow armor, and equip them:

        c_give("handpillow_steelwool", 1)
        c_give("bodypillow_steelwool", 1)

6.  Wait for an evening:

        TheWorld:PushEvent("ms_setphase", "dusk")

7.  Get a *Glove of Challenge* and give it to the *Cozy Bunnyman* with *Steel Wool Pillow* and *Steel Wool Pillow Armor*:

        c_give("yotr_token", 1)

8.  Ensure that he has equipped his pillow armor.

9.  Activate the *Pillow Fight Bell*.

10. During the fight allow the bunnyman to hit you, then unequip your pillow armor and allow him to hit you again.

11. Ensure that the pillow armor reduces the knockback effect.

12. Win the fight, get a *Red Pouch* as the reward, and unwrap it.

13. Ensure that you have got exactly 2 *Lucky Gold Nuggets*.


## *Dreadstone Armor* and *Helm* combination

1.  Get a *Dreadstone Armor* and a *Dreadstone Helm*, and equip them:

        c_give("armordreadstone", 1)
        c_give("dreadstonehat", 1)

2.  Spawn a *Hound* and allow it to damage you:

        c_spawn("hound")

3.  Measure the time to recover 1% of durability of the helm.
4.  Unequip the armor.
5.  Measure the time to recover 1% of durability of the helm again.
6.  Ensure that the former value is 1.5 times less than the latter one.
7.  Measure the time to drain 1 sanity.
8.  Equip the armor back.
9.  Measure the time to drain 1 sanity again.
10. Ensure that the former value approximately equals the latter one.


## Brightshade set bonus

1. Get the armor and the helm, and equip them:

        c_give("armor_lunarplant", 1)
        c_give("lunarplanthat", 1)

2. Spawn a punching bag:

        c_spawn("punchingbag")

3. Get the weapon items:

        c_give("pickaxe_lunarplant", 1)
        c_give("shovel_lunarplant", 1)
        c_give("staff_lunarplant", 1)
        c_give("sword_lunarplant", 1)

4. Hit the punching bag with every weapon and check the damage with the following table.

   | Weapon                | Characteristic | With armor and helm | With helm only |
   |:----------------------|:---------------|:-------------------:|:--------------:|
   | *Brightshade Smasher* | damage         |         50          |       42       |  
   | *Brightshade Shoevel* | damage         |         33          |       27       |
   | *Brightshade Staff*   | bounces        |          7          |        5       |
   | *Brightshade Sword*   | damage         |         76          |       68       |

5. Unequip and equip again the helm. 
6. Check the damage again.


## Void set bonus

1. Get the armor and the helm, and equip them:

        c_give("armor_voidcloth", 1)
        c_give("voidclothhat", 1)

2. Measure the time to drain 1 sanity.
3. Unequip the armor.
4. Measure the time to drain 1 sanity again.
5. Ensure that the former value approximately equals the latter one.


## Punching bags

1. Spawn the punching bags:

        c_spawn("punchingbag")
        c_spawn("punchingbag_shadow")
        c_spawn("punchingbag_lunar")

2. Give them body items of all types and ensure they take them.
