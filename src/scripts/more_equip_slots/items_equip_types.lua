local EQUIP_TYPES = require "more_equip_slots/equip_types"

return {
    -- Life vests
    ["balloonvest"]       = EQUIP_TYPES.LIFEVEST, -- Inflatable Vest

    -- Armors
    ["armor_bramble"]     = EQUIP_TYPES.ARMOR, -- Bramble Husk
    ["armordragonfly"]    = EQUIP_TYPES.ARMOR, -- Scalemail
    ["armordreadstone"]   = EQUIP_TYPES.ARMOR, -- Dreadstone Armor
    ["armorgrass"]        = EQUIP_TYPES.ARMOR, -- Grass Suit
    ["armormarble"]       = EQUIP_TYPES.ARMOR, -- Marble Suit
    ["armorruins"]        = EQUIP_TYPES.ARMOR, -- Thulecite Suit
    ["armor_lunarplant"]  = EQUIP_TYPES.ARMOR, -- Brightshade Armor
    ["armor_sanity"]      = EQUIP_TYPES.ARMOR, -- Night Armour
    ["armor_voidcloth"]   = EQUIP_TYPES.ARMOR, -- Void Robe
    ["armorskeleton"]     = EQUIP_TYPES.ARMOR, -- Bone Armor
    ["armorwood"]         = EQUIP_TYPES.ARMOR, -- Log Suit
    ["bodypillow_petals"]      = EQUIP_TYPES.ARMOR, -- Flowery Pillow Armor
    ["bodypillow_kelp"]        = EQUIP_TYPES.ARMOR, -- Kelpy Pillow Armor
    ["bodypillow_beefalowool"] = EQUIP_TYPES.ARMOR, -- Beefalo Pillow Armor
    ["bodypillow_steelwool"]   = EQUIP_TYPES.ARMOR, -- Steel Wool Pillow Armor

    -- Armors: from the mod "More Armor" (<https://steamcommunity.com/sharedfiles/filedetails/?id=1153998909>)
    ["armor_bone"]        = EQUIP_TYPES.ARMOR, -- Bone Suit
    ["armor_stone"]       = EQUIP_TYPES.ARMOR, -- Stone Suit

    -- Dresses
    ["armorslurper"]     = EQUIP_TYPES.DRESS, -- Belt of Hunger
    ["beargervest"]      = EQUIP_TYPES.DRESS, -- Hibearnation Vest
    ["hawaiianshirt"]    = EQUIP_TYPES.DRESS, -- Floral Shirt
    ["raincoat"]         = EQUIP_TYPES.DRESS, -- Rain Coat
    ["reflectivevest"]   = EQUIP_TYPES.DRESS, -- Summer Frest
    ["sweatervest"]      = EQUIP_TYPES.DRESS, -- Dapper Vest
    ["trunkvest_summer"] = EQUIP_TYPES.DRESS, -- Breezy Vest
    ["trunkvest_winter"] = EQUIP_TYPES.DRESS, -- Puffy Vest
    ["carnival_vest_a"]  = EQUIP_TYPES.DRESS, -- Chirpy Scarf
    ["carnival_vest_b"]  = EQUIP_TYPES.DRESS, -- Chirpy Cloak
    ["carnival_vest_c"]  = EQUIP_TYPES.DRESS, -- Chirpy Capelet

    -- Amulets
    ["amulet"]       = EQUIP_TYPES.AMULET, -- Life Giving Amulet
    ["blueamulet"]   = EQUIP_TYPES.AMULET, -- Chilled Amulet
    ["purpleamulet"] = EQUIP_TYPES.AMULET, -- Nightmare Amulet
    ["orangeamulet"] = EQUIP_TYPES.AMULET, -- The Lazy Forager
    ["greenamulet"]  = EQUIP_TYPES.AMULET, -- Construction Amulet
    ["yellowamulet"] = EQUIP_TYPES.AMULET, -- Magiluminescence

    -- Costumes
    ["costume_doll_body"]       = EQUIP_TYPES.COSTUME, -- Doll Costume
    ["costume_queen_body"]      = EQUIP_TYPES.COSTUME, -- Queen Costume
    ["costume_king_body"]       = EQUIP_TYPES.COSTUME, -- King Costume
    ["costume_blacksmith_body"] = EQUIP_TYPES.COSTUME, -- Blacksmith Costume
    ["costume_mirror_body"]     = EQUIP_TYPES.COSTUME, -- Mirror Costume
    ["costume_tree_body"]       = EQUIP_TYPES.COSTUME, -- Tree Costume
    ["costume_fool_body"]       = EQUIP_TYPES.COSTUME, -- Fool Costume

    -- From the mod "Functional Medal" (<https://steamcommunity.com/sharedfiles/filedetails/?id=1909182187>).
    ["armor_blue_crystal"]      = EQUIP_TYPES.ARMOR,
    ["armor_medal_obsidian"]    = EQUIP_TYPES.ARMOR,
    ["down_filled_coat"]        = EQUIP_TYPES.DRESS,

    -- From the mod "Island Adventures" (<https://steamcommunity.com/sharedfiles/filedetails/?id=1467214795>).
    ["armorseashell"]       = EQUIP_TYPES.ARMOR, -- Seashell Suit (<https://dontstarve.fandom.com/wiki/Seashell_Suit)
    ["armorlimestone"]      = EQUIP_TYPES.ARMOR, -- Limestone Suit (<https://dontstarve.fandom.com/wiki/Limestone_Suit)
    ["armorobsidian"]       = EQUIP_TYPES.ARMOR, -- Obsidian Armor (<https://dontstarve.fandom.com/wiki/Obsidian_Armor>)
    ["armorcactus"]         = EQUIP_TYPES.ARMOR, -- Cactus Armor (<https://dontstarve.fandom.com/wiki/Cactus_Armor>)
    ["armor_snakeskin"]     = EQUIP_TYPES.DRESS, -- Snakeskin Jacket (<https://dontstarve.fandom.com/wiki/Snakeskin_Jacket>)
    ["armor_lifejacket"]    = EQUIP_TYPES.LIFEVEST, -- Life Jacket (<https://dontstarve.fandom.com/wiki/Life_Jacket>)
    ["armor_windbreaker"]   = EQUIP_TYPES.DRESS, -- Windbreaker (<https://dontstarve.fandom.com/wiki/Windbreaker>)
    ["tarsuit"]             = EQUIP_TYPES.DRESS, -- Tar Suit (<https://dontstarve.fandom.com/wiki/Tar_Suit>)
    ["blubbersuit"]         = EQUIP_TYPES.DRESS, -- Blubber Suit (<https://dontstarve.fandom.com/wiki/Blubber_Suit>)

    -- From the mod "[DST] WhaRang" (<https://steamcommunity.com/sharedfiles/filedetails/?id=1108032281>)
    ["wharang_amulet"] = EQUIP_TYPES.AMULET, -- MISSING NAME

    -- From the mod "Aria Crystal (Rebuild)" (<https://steamcommunity.com/sharedfiles/filedetails/?id=2418617371>).
    ["aria_armor_blue"]   = EQUIP_TYPES.ARMOR,  -- Frozen Space-time
    ["aria_armor_green"]  = EQUIP_TYPES.ARMOR,  -- Ayaria Palace
    ["aria_armor_purple"] = EQUIP_TYPES.ARMOR,  -- Enysomn Magic Palace
    ["aria_armor_red"]    = EQUIP_TYPES.ARMOR,  -- Scorching Asylum
    ["aria_seaamulet"]    = EQUIP_TYPES.AMULET, -- Contract of the Crystal Sea
}
