local log = {}


function log.log(level, format, ...)
    print("[dst-more-equip-slots]", level, string.format(format, ...))
end

function log.debug(format, ...)
    log.log("DEBUG", format, ...)
end

function log.info(format, ...)
    log.log("INFO", format, ...)
end

function log.warning(format, ...)
    log.log("WARN", format, ...)
end

function log.error(format, ...)
    log.log("ERROR", format, ...)
end


function log.dump(value, max_depth)
    if max_depth == nil then
        max_depth = 1
    end

    local items = {}

    local function _dump(val, depth)
        local val_type = type(val)
        if val_type == "number" then
            table.insert(items, tostring(val))
        elseif val_type == "boolean" then
            table.insert(items, tostring(val))
        elseif val_type == "nil" then
            table.insert(items, tostring(val))
        elseif val_type == "string" then
            table.insert(items, string.format("%q", val))
        elseif val_type == "table" and depth < max_depth then
            table.insert(items, "{")
            for k, v in pairs(val) do
                local key_type = type(k)
                if key_type == "number" then
                    -- pass
                elseif key_type == "string" then
                    if string.match(k, "^[_A-Za-z][_A-Za-z0-9]*$") ~= nil then
                        table.insert(items, string.format("%s=", k))
                    else
                        table.insert(items, string.format("[%q]=", k))
                    end
                else
                    table.insert(items, string.format("[%s]=", tostring(k)))
                end
                _dump(v, depth + 1)
                if next(val, k) then
                    table.insert(items, ", ")
                end
            end
            table.insert(items, "}")
        elseif val_type == "function" then
            local info = debug.getinfo(val)
            if info.namewhat ~= "" then
                table.insert(items, string.format("[%s %s]", info.namewhat, info.name))
            else
                table.insert(items, string.format("[%s]", tostring(val)))
            end
        else
            table.insert(items, string.format("[%s]", tostring(val)))
        end
    end

    _dump(value, 0)
    return table.concat(items)
end


return log
