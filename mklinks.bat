set PROJECT_DIR=%~dp0
set PROJECT_NAME=dst-more-equip-slots
set DST_DATA_DIR=%HOMEDRIVE%%HOMEPATH%\Documents\Klei\DoNotStarveTogether

set STEAM_LIB_DIR=%1
if "%STEAM_LIB_DIR%"=="" set STEAM_LIB_DIR=C:\Program Files (x86)\Steam
set CLUSTER=%2
if "%CLUSTER%"=="" set CLUSTER=Cluster_1

set DST_APP_DIR=%STEAM_LIB_DIR%\steamapps\common\Don't Starve Together

rmdir     "%DST_APP_DIR%\mods\%PROJECT_NAME%"
mklink /J "%DST_APP_DIR%\mods\%PROJECT_NAME%" "%PROJECT_DIR%src"

mkdir  "%PROJECT_DIR%logs\"
del /Q "%PROJECT_DIR%logs\server_log.txt"
mklink "%PROJECT_DIR%logs\server_log.txt" "%DST_DATA_DIR%\60280137\%CLUSTER%\Master\server_log.txt"
del /Q "%PROJECT_DIR%logs\client_log.txt"
mklink "%PROJECT_DIR%logs\client_log.txt" "%DST_DATA_DIR%\client_log.txt"

pause
